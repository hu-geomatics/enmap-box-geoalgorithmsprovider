from qgis.core import QgsApplication
from enmapboxgeoalgorithms.provider import EnMAPProvider
import enmapboxgeoalgorithms.algorithms

def classFactory(iface):
    return EnMAPPlugin(iface)

class EnMAPPlugin(object):
    def __init__(self, iface):
        self.iface = iface

    def initGui(self):
        self.provider = EnMAPProvider()
        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        pass # todo
        #Processing.removeProvider(self.provider)

