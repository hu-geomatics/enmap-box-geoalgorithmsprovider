=================================================
Welcome to the EnMAP-Box Algorithm documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: GeoAlgorithms:
   :glob:

   ga.rst



.. codeauthor:: Andreas Rabe <andreas.rabe@geo.hu-berlin.de>

.. sectionauthor:: Andreas Rabe <andreas.rabe@geo.hu-berlin.de>
